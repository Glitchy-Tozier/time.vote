-- Your SQL goes here

ALTER TABLE administration
ADD COLUMN deletion_date TIMESTAMPTZ(0) NOT NULL DEFAULT '2023-01-01 00:00:00';

ALTER TABLE administration
ALTER COLUMN deletion_date DROP DEFAULT;