//! Defines the types [`ApiResult`] and [`ApiError`],
//! that are used to make the code in [crate::api] less redundant.

use diesel::result::Error as DieselError;
use rocket::{
    http::Status,
    request::Request,
    response::{self, Responder},
};
use rocket_okapi::{
    gen::OpenApiGenerator,
    okapi::openapi3::{RefOr, Response as OpenApiReponse, Responses},
    response::OpenApiResponderInner,
};
use schemars::Map;
use std::{
    error::Error as StdError,
    fmt::{self, Display, Formatter},
    io::Error as IoError,
    result::Result,
};
use validator::ValidationErrors;

/// A [`Result`] that uses [`ApiError`] as its [`Err()`]-variant.
pub type ApiResult<T> = Result<T, ApiError>;

/// An [`enum`] that contains a variant for every possible Error that can occur within the API.
#[derive(Debug)]
pub enum ApiError {
    StructValidation(ValidationErrors),
    IdValidation,
    DieselNotFound(DieselError),
    DieselOther(DieselError),
    Io(IoError),
    Text(String),
}

impl StdError for ApiError {}

impl Display for ApiError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            ApiError::StructValidation(e) => write!(f, "StructValidation Error: {}", e),
            ApiError::IdValidation => write!(f, "{}", ApiError::IdValidation),
            ApiError::DieselNotFound(e) => write!(f, "DieselNotFound Error: {}", e),
            ApiError::DieselOther(e) => write!(f, "DieselOther Error: {}", e),
            ApiError::Io(e) => write!(f, "Io Error: {}", e),
            ApiError::Text(e) => write!(f, "Text Error: {}", e),
        }
    }
}

impl From<DieselError> for ApiError {
    fn from(e: DieselError) -> Self {
        match e {
            DieselError::NotFound => ApiError::DieselNotFound(e),
            _ => ApiError::DieselOther(e),
        }
    }
}
impl From<ValidationErrors> for ApiError {
    fn from(e: ValidationErrors) -> Self {
        ApiError::StructValidation(e)
    }
}
impl From<IoError> for ApiError {
    fn from(e: IoError) -> Self {
        ApiError::Io(e)
    }
}
impl From<String> for ApiError {
    fn from(e: String) -> Self {
        ApiError::Text(e)
    }
}

impl<'r, 'o: 'r> Responder<'r, 'o> for ApiError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        // log `self` to your favored error tracker, e.g.
        //sentry::capture_error(&self);

        match self {
            Self::StructValidation(e) => {
                println!("{e}");
                Status::UnprocessableEntity.respond_to(req)
            }
            Self::IdValidation => {
                println!("....IdValidation-error");
                Status::Forbidden.respond_to(req)
            }
            Self::DieselNotFound(e) => {
                println!("{e}");
                Status::NotFound.respond_to(req)
            }
            Self::Io(e) => {
                println!("{e}");
                Status::NotFound.respond_to(req)
            }
            _ => {
                println!("Other error");
                Status::InternalServerError.respond_to(req)
            }
        }
    }
}

impl OpenApiResponderInner for ApiError {
    fn responses(
        _generator: &mut OpenApiGenerator,
    ) -> Result<Responses, rocket_okapi::OpenApiError> {
        let mut responses = Map::new();
        /* responses.insert(
            "400".to_string(),
            RefOr::Object(OpenApiReponse {
                description: "\
                [400 Bad Request](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400)\n\
                The server cannot or will not process the request due to something that is perceived to be a client \
                error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).\
                "
                .to_string(),
                ..Default::default()
            }),
        ); */
        responses.insert(
            "403".to_string(),
            RefOr::Object(OpenApiReponse {
                description: "\
                [403 Forbidden](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/403)\n\
                The client does not have access rights to the content; that is, it is unauthorized, \
                so the server is refusing to give the requested resource. Unlike `401` Unauthorized, \
                the client's identity is known to the server.\
                "
                .to_string(),
                ..Default::default()
            }),
        );
        responses.insert(
            "404".to_string(),
            RefOr::Object(OpenApiReponse {
                description: "\
                [404 Not Found](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/404)\n\
                The server can not find the requested resource. In the browser, this means the URL is not recognized. \
                In an API, this can also mean that the endpoint is valid but the resource itself does not exist. \
                Servers may also send this response instead of `403` Forbidden to \
                hide the existence of a resource from an unauthorized client. \
                This response code is probably the most well known due to its frequent occurrence on the web.\
                "
                .to_string(),
                ..Default::default()
            }),
        );
        responses.insert(
            "422".to_string(),
            RefOr::Object(OpenApiReponse {
                description: "\
                [422 Unprocessable Entity](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/422)\n\
                The request was well-formed but was unable to be followed due to semantic errors.\
                ".to_string(),
                ..Default::default()
            }),
        );
        responses.insert(
            "500".to_string(),
            RefOr::Object(OpenApiReponse {
                description: "\
                [500 Internal Server Error](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/500)\n\
                This response is given when something wend wrong on the server.\
                ".to_string(),
                ..Default::default()
            }),
        );
        Ok(Responses {
            responses,
            ..Default::default()
        })
    }
}
