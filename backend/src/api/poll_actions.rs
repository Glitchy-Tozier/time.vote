//! Poll-specific requests that interact with the database by creating/changing/deleting data.

use super::poll_sites::*;

use crate::database::validate_poll_admin;
use crate::errors::ApiResult;
use crate::helpers::{get_random_id, get_unique_id};
use crate::models::{
    db_models::*,
    helper_models::*,
    schema::{administration as admn, polls, users, votes},
};
use crate::DbConn;

use diesel::prelude::*;
use rocket::{delete, patch, post, serde::json::Json};
use rocket_okapi::openapi;
use rocket_sync_db_pools::diesel::insert_into;
use validator::Validate;

/// Creates a new [`Poll`] using the supplied [`SetupData`].
///
/// Returns the link that should be navigated to next.
/// The first part of the link is the `poll_id`, the last part is the `admin_id`.
#[openapi(tag = "Poll actions")]
#[post("/create", format = "json", data = "<setup_data>", rank = 3)]
pub(crate) async fn create_poll(conn: DbConn, setup_data: Json<SetupData>) -> ApiResult<String> {
    // Get values
    let setup_data = setup_data.into_inner();
    setup_data.validate()?;
    //let admin_email = setup_data.admin_email.clone();

    let (poll_id, admin_id): (String, String) = conn
        .run(
            |c: &mut diesel::PgConnection| -> ApiResult<(String, String)> {
                let poll_id_is_unique = |id: &str| -> ApiResult<bool> {
                    let is_unique = polls::table.find(id).execute(c)? == 0;
                    Ok(is_unique)
                };
                let poll_id = get_unique_id(poll_id_is_unique)?;
                let poll = Poll::new(&poll_id, setup_data);

                let admin_id: String = get_random_id(); // No need to check for uniqueness as it's the first user.
                let admin_user = User::new_admin(&poll.poll_id, &admin_id /* , admin_email */);
                let administration_data = Administration::new(&poll_id, &admin_id)?;

                // Save to database
                let _ = insert_into(polls::table).values(poll).execute(c)?;
                let _ = insert_into(users::table).values(admin_user).execute(c)?;
                let _ = insert_into(admn::table)
                    .values(administration_data)
                    .execute(c)?;

                Ok((poll_id, admin_id))
            },
        )
        .await?;

    let link = uri!(admin_links_page(poll_id, admin_id)).to_string();
    Ok(link)
}

/// Updates a [Poll]'s metadata according to the supplied values in [`SetupData`].
///
/// Returns the link that should be navigated to next.
/// The first part of the link is the `poll_id`, the last part is the `admin_id`.
#[openapi(tag = "Poll actions")]
#[patch(
    "/<poll_id>/edit/<admin_id>",
    format = "json",
    data = "<setup_data>",
    rank = 2
)]
pub(crate) async fn edit_poll(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
    setup_data: Json<SetupData>,
) -> ApiResult<String> {
    let setup_data = setup_data.into_inner();
    setup_data.validate()?;

    //let admin_email = setup_data.admin_email.clone();
    let poll = Poll::new(&poll_id, setup_data);

    let poll_id_clone = poll_id.clone();
    let admin_id_clone = admin_id.clone();

    conn.run(move |c| -> ApiResult<()> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Save changes to Poll
        poll.save_changes::<Poll>(c)?;
        // Save changes to the admin's email-address
        let admin: User = users::table.find((&poll_id, &admin_id)).first(c)?;
        //admin.email = admin_email;
        admin.save_changes::<User>(c)?;

        Ok(())
    })
    .await?;

    let link = uri!(admin_links_page(poll_id_clone, admin_id_clone)).to_string();
    Ok(link)
}

/// Deletes a the [`Poll`] that corresponds to the supplied `poll_id`.
///
/// Returns the link that should be navigated to next; the thank-you-website.
#[openapi(tag = "Poll actions")]
#[delete("/<poll_id>/delete-poll/<admin_id>", rank = 3)]
pub(crate) async fn delete_poll(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<String> {
    conn.run(move |c| -> ApiResult<()> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Delete all votes that belong to this poll
        diesel::delete(votes::table.filter(votes::poll_id.eq(&poll_id))).execute(c)?;
        // Delete the administration-data that belongs to this poll
        diesel::delete(admn::table.find(&poll_id)).execute(c)?;
        // Delete all users that participated in this poll
        diesel::delete(users::table.filter(users::poll_id.eq(&poll_id))).execute(c)?;
        // Delete the poll
        diesel::delete(polls::table.find(&poll_id)).execute(c)?;

        Ok(())
    })
    .await?;

    let link = "/thanks".to_string();
    Ok(link)
}

/// Called to insert a user's [`Vote`] into a specific poll.
///
/// Returns the link that should be navigated to next.
/// The first part of the link is the `poll_id`, the last part is the `vote_id`.
#[openapi(tag = "Vote actions")]
#[post("/<poll_id>", format = "json", data = "<json_uservote>", rank = 4)]
pub(crate) async fn add_vote(
    conn: DbConn,
    poll_id: String,
    json_uservote: Json<BareUserVote>,
) -> ApiResult<String> {
    let uservote = json_uservote.into_inner();
    uservote.validate()?;

    let poll_id_clone = poll_id.clone();
    let vote_id = conn
        .run(move |c: &mut diesel::PgConnection| -> ApiResult<String> {
            // Create a new User with an unique ID
            let user_id_is_unique = |id: &str| -> ApiResult<bool> {
                let is_unique = users::table.find((&poll_id, id)).execute(c)? == 0;
                Ok(is_unique)
            };
            let user_id = get_unique_id(user_id_is_unique)?;
            let user = User::new(&poll_id, &user_id, &uservote);

            // Create a new Vote with an unique ID
            let vote_id_is_unique = |id: &str| -> ApiResult<bool> {
                let is_unique = votes::table.find((&poll_id, id)).execute(c)? == 0;
                Ok(is_unique)
            };
            let vote_id = get_unique_id(vote_id_is_unique)?;
            let vote = Vote::new(&poll_id, &vote_id, &user_id, &uservote);

            // Insert User & Vote
            insert_into(users::table).values(user).execute(c)?;
            insert_into(votes::table).values(vote).execute(c)?;

            Ok(vote_id)
        })
        .await?;

    let link = uri!(vote_links_page(poll_id_clone, vote_id)).to_string();
    Ok(link)
}

/// Updates the [`Vote`] with the supplied `vote_id` in the [`Poll`] that corresponds to the supplied `poll_id`.
///
/// Returns the link that should be navigated to next.
/// The first part of the link is the `poll_id`, the last part is the `vote_id`.
#[openapi(tag = "Vote actions")]
#[patch(
    "/<poll_id>/update/<vote_id>",
    format = "json",
    data = "<uservote>",
    rank = 2
)]
pub(crate) async fn update_vote(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
    uservote: Json<BareUserVote>,
) -> ApiResult<String> {
    let new_uservote = uservote.into_inner();
    new_uservote.validate()?;

    let poll_id_clone = poll_id.clone();
    let vote_id_clone = vote_id.clone();

    conn.run(move |c: &mut diesel::PgConnection| -> ApiResult<()> {
        let mut vote: Vote = votes::table.find((&poll_id, &vote_id)).first(c)?;
        let mut user: User = users::table.find((&poll_id, &vote.voter_id)).first(c)?;

        // Save changes to the vote
        vote.comment = new_uservote.comment;
        vote.availability = new_uservote.availability;
        vote.admin_edited = new_uservote.admin_edited;
        vote.save_changes::<Vote>(c)?;
        // Save changes to the user's data
        user.name = new_uservote.name;
        //user.email = new_uservote.email;
        user.save_changes::<User>(c)?;

        Ok(())
    })
    .await?;

    let link = uri!(vote_links_page(poll_id_clone, vote_id_clone)).to_string();
    Ok(link)
}

/// Changes the [`Vote`]'s property [`admin_edited`] to false.
///
/// Returns nothing exept the [`ApiResult`].
#[openapi(tag = "Vote actions")]
#[patch("/<poll_id>/unset_admin_edited/<vote_id>", rank = 2)]
pub(crate) async fn unset_admin_edited(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<()> {
    conn.run(move |c: &mut diesel::PgConnection| -> ApiResult<()> {
        let mut vote: Vote = votes::table.find((&poll_id, &vote_id)).first(c)?;
        // Save changes to the vote
        vote.admin_edited = false;
        vote.save_changes::<Vote>(c)?;

        Ok(())
    })
    .await?;

    Ok(())
}

/// Deletes the [`Poll`] that corresponds to the supplied `poll_id`.
///
/// Returns the link that should be navigated to next; the thank-you-website.
#[openapi(tag = "Vote actions")]
#[delete("/<poll_id>/delete-vote/<vote_id>", rank = 3)]
pub(crate) async fn delete_vote(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<String> {
    conn.run(move |c| -> ApiResult<()> {
        // Delete the vote
        let deleted_vote: Vote =
            diesel::delete(votes::table.find((&poll_id, &vote_id))).get_result(c)?;

        // Delete the user which corresponds to the vote IF the user is not the admin
        let admin_id: String = admn::table.find(&poll_id).select(admn::admin_id).first(c)?;
        if deleted_vote.voter_id != admin_id {
            diesel::delete(users::table.find((&poll_id, deleted_vote.voter_id))).execute(c)?;
        }

        Ok(())
    })
    .await?;

    let link = "/thanks".to_string();
    Ok(link)
}
