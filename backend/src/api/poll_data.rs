//! Poll-specific requests that are used to get pure data from the server.

use crate::database::validate_poll_admin;
use crate::errors::ApiResult;
use crate::models::{
    db_models::*,
    helper_models::*,
    schema::{polls, users, votes},
};
use crate::DbConn;

use diesel::prelude::*;
use rocket::{get, serde::json::Json};
use rocket_okapi::openapi;

/// Returns [`SetupData`]. Used to let the [Poll]'s admin edit the metadata.
#[openapi(tag = "Data getters")]
#[get("/<poll_id>/poll-title", rank = 3)]
pub(crate) async fn get_poll_title(conn: DbConn, poll_id: String) -> ApiResult<Json<String>> {
    conn.run(move |c| -> ApiResult<String> {
        let poll: Poll = polls::table.find(&poll_id).first(c)?;
        Ok(poll.title)
    })
    .await
    .map(Json)
}

/// Returns [`SetupData`]. Used to let the [Poll]'s admin edit the metadata.
#[openapi(tag = "Data getters")]
#[get("/<poll_id>/setup-data/<admin_id>", rank = 3)]
pub(crate) async fn get_poll_setupdata(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<Json<SetupData>> {
    conn.run(move |c| {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        let poll: Poll = polls::table.find(&poll_id).first(c)?;
        /* let admin_email = users::table
        .find((&poll_id, &admin_id))
        .select(users::email)
        .first(c)?; */

        Ok(SetupData::from_poll(&poll /* , &admin_email */))
    })
    .await
    .map(Json)
}

/// Returns the [`Poll`]'s data. Used to provide users with the information needed to make a vote.
#[openapi(tag = "Data getters")]
#[get("/<poll_id>/poll-data", rank = 3)]
pub(crate) async fn get_poll_data(conn: DbConn, poll_id: String) -> ApiResult<Json<Poll>> {
    conn.run(move |c| -> ApiResult<Poll> {
        let poll: Poll = polls::table.find(&poll_id).first(c)?;

        Ok(poll)
    })
    .await
    .map(Json)
}

/// Returns the [`Vote`] which corresponds to the supplied `vote_id`.
/// Used to allow for editing the vote.
#[openapi(tag = "Data getters")]
#[get("/<poll_id>/vote-data/<vote_id>", rank = 3)]
pub(crate) async fn get_vote_data(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<Json<BareUserVote>> {
    conn.run(
        move |c: &mut diesel::PgConnection| -> ApiResult<BareUserVote> {
            let vote: Vote = votes::table.find((&poll_id, &vote_id)).first(c)?;
            let user: User = users::table.find((&poll_id, &vote.voter_id)).first(c)?;

            Ok(BareUserVote::from_user_and_vote(&user, &vote))
        },
    )
    .await
    .map(Json)
}

/// Returns all poll-data.
/// Used by the admin to view the results and decide on when the event should take place.
#[openapi(tag = "Data getters")]
#[get("/<poll_id>/results-data/<admin_id>", rank = 3)]
pub(crate) async fn get_results_data(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<Json<PollResults>> {
    conn.run(move |c| -> ApiResult<PollResults> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Get poll with the desired `poll_id`
        let poll: Poll = polls::table.find(&poll_id).first(c)?;
        // Get corresponding votes & users
        let votes_with_users: Vec<(Vote, User)> = votes::table
            .filter(votes::poll_id.eq(&poll_id))
            .inner_join(
                users::table.on(votes::poll_id
                    .eq(users::poll_id)
                    .and(votes::voter_id.eq(users::user_id))),
            )
            .load(c)?;
        // Combine votes & users into `BareUserVote`s
        let uservotes: Vec<ResultsUserVote> = votes_with_users
            .into_iter()
            .map(|(vote, user)| ResultsUserVote::from_user_and_vote(&user, &vote))
            .collect();

        Ok(PollResults { poll, uservotes })
    })
    .await
    .map(Json)
}
