//! Functions relating to open_api.

use rocket_okapi::swagger_ui::SwaggerUIConfig;

pub(crate) fn get_docs() -> SwaggerUIConfig {
    // use rocket_okapi::settings::UrlObject;

    SwaggerUIConfig {
        url: "/openapi.json".to_string(),
        ..Default::default()
    }
}
