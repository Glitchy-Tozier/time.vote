#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use rocket_dyn_templates::Template;
use rocket_okapi::swagger_ui::make_swagger_ui;

mod api;
mod database;
mod error_catchers;
mod errors;
mod helpers;
mod models;
#[cfg(test)]
mod tests;

use database::DbConn;
// Prepares automatic checks for migrations.
pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations");
//embed_migrations!("./migrations");

/// The analog to the usual `fn main()`.
#[launch]
fn rocket() -> _ {
    // Automatically run db-migrations
    let mut db_conn = database::establish_manual_connection();
    db_conn
        .run_pending_migrations(MIGRATIONS)
        .unwrap_or_else(|e| panic!("Error running migrations: {}", e));

    let mut rocket_build = rocket::build()
        .mount("/", api::SlashRemover)
        .register("/", error_catchers::catchers())
        .attach(DbConn::fairing())
        .attach(Template::fairing());

    let developing = rocket::Config::default() == rocket::Config::debug_default();
    rocket_build = if developing {
        // If debugging, use additional routes and add openApi
        rocket_build
            .mount("/", api::dev_routes())
            .mount("/swagger", make_swagger_ui(&api::open_api::get_docs()))
    } else {
        // In release, only show the "release_routes"
        rocket_build.mount("/", api::release_routes())
    };

    rocket_build
}
