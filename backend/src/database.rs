//! Functions and trait-implementations that make interacting with the database more convenient.

use crate::errors::{ApiError, ApiResult};
use crate::models::schema::{administration as admn, votes};
use crate::models::{db_models::Poll, schema::polls};

use diesel::{prelude::*, Connection, PgConnection, QueryDsl};
use dotenvy::dotenv;
use rocket_okapi::{
    gen::OpenApiGenerator,
    request::{OpenApiFromRequest, RequestHeaderInput},
};
use rocket_sync_db_pools::database;
use std::env;

/// Manually establishes a connection to the database, using [`diesel`]'s methods and the provided `.env`-file.
pub fn establish_manual_connection() -> PgConnection {
    // Get variables from `.env`-file
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let mut obfuscated_db_url = database_url.clone();
    obfuscated_db_url.replace_range(
        13 + obfuscated_db_url[12..].find(':').unwrap()..obfuscated_db_url.find('@').unwrap(),
        "[password]",
    );

    println!("Trying to connect to database-url: {obfuscated_db_url}");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|e| panic!("\n\nError connecting to {obfuscated_db_url}: {e}\n\n"))
}

/// Connection to the server's [diesel]-PostgreSQL-database.
#[database("voteon_date_db")]
pub struct DbConn(diesel::PgConnection);

impl DbConn {
    /// 1. Checks whether the given `admin_id` is valid.
    /// 2. Loads the [`Poll`] with the given `poll_id` from the database and returns it.
    pub async fn validate_poll_admin(&self, poll_id: String, admin_id: String) -> ApiResult<()> {
        self.run(move |c| -> ApiResult<()> { validate_poll_admin(&poll_id, &admin_id, c) })
            .await
    }

    /// 1. Checks whether the given `vote_id` points to an existing vote.
    /// 2. Loads the [`Poll`] with the given `poll_id` from the database and returns it.
    pub async fn validate_vote_id(&self, poll_id: String, vote_id: String) -> ApiResult<()> {
        self.run(move |c| -> ApiResult<()> { validate_vote_id(&poll_id, &vote_id, c) })
            .await
    }

    /// Loads the [`Poll`] with the given `poll_id` from the database and returns it.
    pub async fn get_poll(&self, poll_id: String) -> ApiResult<Poll> {
        self.run(|c| -> ApiResult<Poll> {
            let poll: Poll = polls::table.find(poll_id).first(c)?;
            Ok(poll)
        })
        .await
    }
}

/// Checks whether the the provided `admin_id` actually is the correct ID for a poll.
pub fn validate_poll_admin(
    poll_id: &str,
    provided_admin_id: &str,
    c: &mut diesel::PgConnection,
) -> ApiResult<()> {
    // Find the correct `admin_id` for the poll with the given `poll_id`
    let real_admin_id: String = admn::table.find(&poll_id).select(admn::admin_id).first(c)?;

    // Check whether the provided `admin_id` matches the correct one
    match provided_admin_id == real_admin_id {
        true => Ok(()),
        false => Err(ApiError::IdValidation),
    }
}

/// Checks whether the given `vote_id` points to an existing vote.
pub fn validate_vote_id(
    poll_id: &str,
    vote_id: &str,
    c: &mut diesel::PgConnection,
) -> ApiResult<()> {
    // Find the correct `admin_id` for the poll with the given `poll_id`
    let found_vote_count: usize = votes::table.find((&poll_id, &vote_id)).execute(c)?;

    // Check whether the provided `admin_id` matches the correct one
    match found_vote_count {
        0 => Err(ApiError::IdValidation),
        1 => Ok(()),
        _ => Err(ApiError::Text(
            "`validate_vote_id()` found more than one vote.".to_string(),
        )),
    }
}

impl<'r> OpenApiFromRequest<'r> for DbConn {
    /// Implementation required by OkApi. See [Here](https://github.com/GREsau/okapi#faq).
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(RequestHeaderInput::None)
    }
}
