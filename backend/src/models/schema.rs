// @generated automatically by Diesel CLI.
// But edited by me due to this: https://diesel.rs/guides/migration_guide.html#2-0-0-nullability-of-array-elements

diesel::table! {
    administration (poll_id) {
        #[max_length = 7]
        poll_id -> Varchar,
        #[max_length = 7]
        admin_id -> Varchar,
        creation_date -> Timestamptz,
        deletion_date -> Timestamptz,
    }
}

diesel::table! {
    polls (poll_id) {
        #[max_length = 7]
        poll_id -> Varchar,
        #[max_length = 50]
        title -> Varchar,
        #[max_length = 200]
        instruction -> Varchar,
        datetime_interval -> Int2,
        offered_datetimes -> Array<Timestamptz>,
        #[max_length = 25]
        theme -> Varchar,
    }
}

diesel::table! {
    users (poll_id, user_id) {
        #[max_length = 7]
        poll_id -> Varchar,
        #[max_length = 7]
        user_id -> Varchar,
        #[max_length = 50]
        name -> Varchar,
    }
}

diesel::table! {
    votes (poll_id, vote_id) {
        #[max_length = 7]
        poll_id -> Varchar,
        #[max_length = 7]
        vote_id -> Varchar,
        #[max_length = 7]
        voter_id -> Varchar,
        #[max_length = 200]
        comment -> Nullable<Varchar>,
        availability -> Array<Timestamptz>,
        admin_edited -> Bool,
    }
}

diesel::joinable!(administration -> polls (poll_id));
diesel::joinable!(users -> polls (poll_id));
diesel::joinable!(votes -> polls (poll_id));

diesel::allow_tables_to_appear_in_same_query!(administration, polls, users, votes,);
