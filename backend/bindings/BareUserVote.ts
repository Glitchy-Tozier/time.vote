// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.

export interface BareUserVote {
  name: string;
  comment: string | null;
  availability: Array<string>;
  adminEdited: boolean;
}
