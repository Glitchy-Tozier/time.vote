## Run

You can run the project by opening the root folder in a terminal and using the following commands:

```bash
# first, install the dependencies
npm install

# then run the project
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

Remember you can also access the site through any device on your network. This means that if you visit `192.168.0.XX` on your phone, the project will show up.
<br/>
<br/>

## Building

To create a production version of the app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.
