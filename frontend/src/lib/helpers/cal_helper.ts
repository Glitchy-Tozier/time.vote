import { addDays, differenceInDays, subDays } from "date-fns";

/**
 * Returns an array of Dates.
 * The numbers start from `start` and end with (but do not include) `end`.
 */
function getDayRange(start: Date, end: Date) {
    const dates = <Date[]>[];
    const current = new Date(start);

    while (current < end) {
        dates.push(new Date(current));
        current.setDate(current.getDate() + 1);
    }
    return dates;
}

/**
 * Returns an array of numbers, representing the hours.
 * The numbers start from `startingHour` and end with (but do not include) `endingHour`.
 */
function getHourRange(
    startingHour: number,
    endingHour: number,
    interval: number
): number[] {
    if (interval < 60 * 24) {
        const hours = <number[]>[];
        let currentHour = startingHour;

        while (currentHour < endingHour) {
            hours.push(currentHour);
            currentHour++;
        }
        return hours;
    } else {
        // If we want call this function but have an interval of `DAY`, only return "hour 0".
        return [0];
    }
}
/**
 * Returns an array of numbers, representing the minutes.
 * The numbers start from 0 and end with the last value that doesn't extend into the next hour.
 */
function getMinuteRange(interval: number): number[] {
    const minutes = <number[]>[];
    let currentMinute = 0;

    while (currentMinute < 60) {
        minutes.push(currentMinute);
        currentMinute += interval;
    }
    return minutes;
}

/**
 * Extracts the days that should be displayed in the calendar from `choosableDatetimeVals`
 * Days missing will be filled up, to a certain degree.
 */
function getCalendarDays(
    choosableDatetimes: Date[],
    timeInterval: number
): Date[] {
    const sortedDays: Date[] = choosableDatetimes
        // Get the unique days' values
        .reduce((unique: number[], date: Date) => {
            const dayVal = new Date(date.toDateString()).valueOf();
            if (!unique.includes(dayVal)) {
                unique.push(dayVal);
            }
            return unique;
        }, [])
        // Sort the days
        .sort()
        // Turn them into actual `Date`s
        .map((val: number) => new Date(val));

    const calendarDays = <Date[]>[];

    sortedDays.forEach((date) => {
        if (calendarDays.length > 0) {
            const inBetweenDays = <Date[]>[];

            let currentD: Date = calendarDays[calendarDays.length - 1];
            let nextD: Date = addDays(currentD, 1);

            // Find the days that come between this and the previous date
            while (nextD.valueOf() !== date.valueOf()) {
                inBetweenDays.push(nextD);
                currentD = nextD;
                nextD = addDays(currentD, 1);
            }

            // In certain cases, reduce the `inBetweenDays` to 3, using
            // `new Date(0) as a placeholder for all cut-off days in the middle.
            if (inBetweenDays.length <= 3 || timeInterval === 60 * 24) {
                // Keep all `inBetweenDays`
                calendarDays.push(...inBetweenDays);
            } else {
                // Cut off the middle `inBetweenDays`
                calendarDays.push(
                    ...[
                        inBetweenDays[0],
                        new Date(0),
                        inBetweenDays[inBetweenDays.length - 1],
                    ]
                );
            }
        }
        calendarDays.push(new Date(date));
    });

    // If the time-interval is DAY, fill up the days at the start & end of the list
    // until we start on monday and end on sunday.
    if (timeInterval === 60 * 24) {
        while (calendarDays[0].getDay() !== 1) {
            const firstDay = calendarDays[0];
            const prevDay = subDays(firstDay, 1);
            calendarDays.unshift(prevDay);
        }
        while (calendarDays[calendarDays.length - 1].getDay() !== 0) {
            const lastDay = calendarDays[calendarDays.length - 1];
            const nextDay = addDays(lastDay, 1);
            calendarDays.push(nextDay);
        }
    }
    return calendarDays;
}

function getCalendarHourRange(
    choosableDatetimes: Date[],
    timeInterval: number
): number[] {
    const rangeParams = {
        earliestHour: choosableDatetimes[0].getHours(),
        latestHour: choosableDatetimes[0].getHours(),
    };
    // Grab earlist and latest found hour from choosable datetimes
    choosableDatetimes.forEach((date: Date) => {
        const hour = date.getHours();
        if (hour < rangeParams.earliestHour) {
            rangeParams.earliestHour = hour;
        } else if (hour > rangeParams.latestHour) {
            rangeParams.latestHour = hour;
        }
    });

    return getHourRange(
        rangeParams.earliestHour,
        rangeParams.latestHour + 1,
        timeInterval
    );
}

/** Returns the (`<time>`-) datetime-string of how many Days have passed. */
function getSkipDatetime(prevDay: Date, nextDay: Date): string {
    const skipStart = addDays(prevDay, 1),
        skipEnd = nextDay,
        skippedDays = differenceInDays(skipEnd, skipStart);

    return `PD${skippedDays}`;
}

export {
    getDayRange,
    getHourRange,
    getMinuteRange,
    getCalendarDays,
    getCalendarHourRange,
    getSkipDatetime,
};
